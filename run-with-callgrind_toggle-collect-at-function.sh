if [ $# -ne 2 ]
  then
    echo "Invalid argument count"
fi

valgrind --tool=callgrind "--toggle-collect=$1" $2
