if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
fi

valgrind --collect-atstart=no --tool=callgrind ./$@
