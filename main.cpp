#include <valgrind/callgrind.h>
#include <vector>

using GrowingDatastruct = std::vector<int>;

void not_profiled_function(GrowingDatastruct& data) {
  data.push_back(data.size());
  return ;
}

void profiled_function(GrowingDatastruct& data) {
  data.push_back(data.size());
  return ;
}

int main()
{
  GrowingDatastruct data;

  not_profiled_function(data);
  profiled_function(data);
  not_profiled_function(data);
  return (0);
}
